import {Fragment} from 'react'

//components
import {Pie} from 'react-chartjs-2'

//helper
import toNum from '../helpers/toNum'

export default function topTen({topTenCountries}){

	console.log(topTenCountries)

	return (
		<Fragment>			<h1 className="text-center">Top 10 Countries Per Cases</h1>
			<Pie 
				data={{

					datasets:[{

						// the pie charts data and reqires an array of numbers
						data: [
						toNum(topTenCountries[0].cases),
						toNum(topTenCountries[1].cases),
						toNum(topTenCountries[2].cases),
						toNum(topTenCountries[3].cases),
						toNum(topTenCountries[4].cases),
						toNum(topTenCountries[5].cases),
						toNum(topTenCountries[6].cases),
						toNum(topTenCountries[7].cases),
						toNum(topTenCountries[8].cases),
						toNum(topTenCountries[9].cases),
						

						],
						//The color of each piece is according to the order of the items in the data array.
						backgroundColor: [

							"#fec5bb",
							"#fcd5ce",
							"#fae1dd",
							"#f8edeb",
							"#e8e8e4",
							"#d8e2dc",
							"#ece4db",
							"#ffe5d9",
							"#ffd7ba",
							"#fec89a"

							]

					}],
					//The label of each piece is according to the order of the items in the data array.
					labels: [
						topTenCountries[0].country_name,
						topTenCountries[1].country_name,
						topTenCountries[2].country_name,
						topTenCountries[3].country_name,
						topTenCountries[4].country_name,
						topTenCountries[5].country_name,
						topTenCountries[6].country_name,
						topTenCountries[7].country_name,
						topTenCountries[8].country_name,
						topTenCountries[9].country_name,
						

					]
				}}

			/>
		</Fragment>

		)
}

export async function getStaticProps(){

  //fetch data from our rapidAPI
  const res = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php',{

      "method": "GET",
      "headers": {

        "x-rapidapi-host":"coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key":"6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
      }

  })

  const data = await res.json()

  const countriesStats = data.countries_stat

  // top 10 loop
  /*
  	const topTenCountries = []

  	for (let i = 0; i < 10; i++) {
  		topTenCountries.push(countriesStats[i])
  	}
  */

  //top 10 arr method
  const topTenCountries = countriesStats.slice(0,10);


  return {

  	props: {

  		topTenCountries
  	}

  }

}
