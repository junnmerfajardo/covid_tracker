import {Fragment} from 'react'
import {Jumbotron,Container,Row,Col} from 'react-bootstrap'

//component
import DoughnutChart from '../components/DoughnutChart'

//helpers
import toNum from '../helpers/toNum'

export default function country({country}){

	console.log(country)

	const stats = {

		criticals: toNum(country.serious_critical),
		deaths: toNum(country.deaths),
		recovered: toNum(country.total_recovered),
		active: toNum(country.active_cases),
		cases: toNum(country.cases)
	}

	return (
		<Fragment>
			<Jumbotron className="mt-5">
				<Container className="mb-4">
					<Row>
						<Col>
							<h1 className="text-center">{country.country_name}</h1>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col sm={5}>
							
						<p>Total Cases: {country.cases}</p>
						<p>Deaths: {country.deaths}</p>
						<p>Recovered: {country.total_recovered}</p>
						<p>Criticals: {country.serious_critical}</p>
						<p>Active: {country.active_cases}</p>
						<p>Total Test: {country.total_tests}</p>
						
						</Col>
						<Col sm={7}>

						<DoughnutChart stats={stats}/>

						</Col>
					</Row>
				</Container>
			</Jumbotron>
		</Fragment>




		)
		

}
/*
getStaticPaths is a unique NextJS function. It is used in conjuction with getStaticProps. getStaticPaths create dynamic routes for dynamically created pages
*/
export async function getStaticPaths(){

	//fetch data from our rapidAPI
  const res = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php',{

      "method": "GET",
      "headers": {

        "x-rapidapi-host":"coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key":"6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
      }

  })

  const data = await res.json()

  //We're going to create paths for each countries in the countries_stat array.
  const paths = data.countries_stat.map(country => {

  	return ({

  		params: {id: country.country_name}

  	})

  })

  // define paths.
  // console.log(paths)

  return {paths, fallback:false}
	/*
		paths contain all the defined paths to the dynamically created pages:
		/USA - you will be able to find a page for USA

		fallback is the page that nextjs will redirect user if they try to go to a path that is undefined or unspecified in the paths.

		fallback:false - nextjs will show the default 404page

		fallback:true - nextjs will show your custom 404 page

	*/

}

/*
	getStaticProps when used in conjunction with getStaticPaths recieves the params of the current route/ endpoint. This will allow us to get the specific data of a country.
*/

export async function getStaticProps({params}){

	// console.log(params)

	//fetch data from our rapidAPI
  const res = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php',{

      "method": "GET",
      "headers": {

        "x-rapidapi-host":"coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key":"6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
      }

  })

  const data = await res.json()

  //gets the details for the particulat country by using the find() method in the array to search for object/element with the same country_name and id property for our params

  const country = data.countries_stat.find(country =>country.country_name === params.id)
  console.log(country)

  return {

  	props: {

  		country

  	}
  }

}