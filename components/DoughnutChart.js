import {Doughnut} from 'react-chartjs-2'

export default function DougnutChart({stats}){

	const {criticals,deaths,recovered,active,cases} = stats

	return(

			<Doughnut data={{

				datasets: [{

					data: [cases,criticals,recovered,deaths,active],
					backgroundColor:[
					"#e8e8e4",
					"#fec89a",
					"#d8e2dc",
					"#ece4db",
					"#ffd7ba"

					]
				}],
				labels: [
				'Cases',
				'Criticals',
				'Recoveries',
				'Deaths',
				'Active'
				]

			}}
			/>


		)
}

"#d8e2dc",
"#ece4db",

"#ffd7ba",
"#fec89a"