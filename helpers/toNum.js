/*This helper toNum, will sanitize the data/statistic of the countries. it will remove the comma and convert the string into number*/

export default function toNum(str){

	//this function will receive a string and return a number.
	//convert a string into an array by using a spread operator.
	const arr = [...str]

	//filter out the commas, out of the numerical string.
	const filteredArr = arr.filter(element => {

	//iterated over all the elements, items in an array and passes the items that pass a condition into a new array. Unlike find(), filter iterated over all items, but in find() it stops when an item has passed the condition.

		return element !== ","

	})

	//reduce the array into a single string and returned will be parsed into an integer.
	return parseInt(filteredArr.reduce((x,y)=>{

		/*
			reduce():
			on the first iteration:

			x is the first item in the array
			y is the second item in the array

			on the second iteration:

			x is the value of the addition in the first iteration
			y is the next item in the array

			NOTE: Strings do not add, instead it concatinantes.

			2
			9
			29
			7
			297
			4
			2974
			4
			29744
			6
			297446
			5
			2974465
			2
			29744652 - reduce to a single string. This string will then be parsed as integer.

			lastly parseInt("29744652")

		*/

		// console.log(x)
		// console.log(y)

		return x + y
	}))
}